
import React from 'react';
import './Formulaire.css';
import { IonButton ,IonCheckbox, IonItem, IonLabel,IonInput } from '@ionic/react'

interface ContainerProps { }

const Formulaire: React.FC<ContainerProps> = () => {

  function login(event : any){
    event.preventDefault();
    let form_login = new FormData(event.target);
    let xhr = new XMLHttpRequest();
    xhr.open("POST","http://192.168.231.196/login",true);
    xhr.onreadystatechange = function(){
        if(this.readyState === 4 && this.status === 200){
          console.log(this.responseText);
            if(this.responseText !== "Non connecte"){
                let token = this.responseText;
                localStorage.setItem("token",token);
                alert("Vous êtes connecté !");
                window.location.href="../listeAvion";
                
            }else
                alert("Utilisateur ou mot de passe erroné !");
        }
    };
    xhr.send(form_login);
  }


  return (

    <div className="container">
    <form className="login" onSubmit={login}>
      <IonItem>
        <IonLabel position="floating">Nom de la société :</IonLabel>
        <IonInput type={"text"} name={"nom"} placeholder={"Air Madagascar"}/>
      </IonItem>
      <IonItem>
        <IonLabel position="floating">Mot de passe</IonLabel>
        <IonInput type={"password"} name={"mdp"} placeholder={"avion"}/>
      </IonItem>
        <IonItem lines="none">
        <IonLabel>Remember me</IonLabel>
        <IonCheckbox defaultChecked={true} slot="start" />
      </IonItem>
      <IonButton className="ion-margin-top" type="submit" expand="block">
      Login
    </IonButton>

    <IonItem routerLink="../listeAvion">
    <IonLabel>Retour</IonLabel>
    </IonItem>
  </form>
  </div>
  );
};

export default Formulaire;