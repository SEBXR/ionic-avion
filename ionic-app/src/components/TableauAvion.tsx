import { IonItem, IonLabel, IonList, IonNote } from "@ionic/react";
import {urlWS} from "./url"
import useFetch from "./useFetch";
interface AvionProps { }

const TableauAvion: React.FC<AvionProps> = () => {
    const {data} = useFetch(urlWS+"/avionsAssurance")
    if (!data)
        return <h1>Loading</h1>
    else {
        return (
            <div className="tableau">
                <IonList>
                    {
                        data.map((avion: any) => {
                            let link = "/detailAvion?id=" + avion.id
                            return (
                                <IonItem routerLink={link}>
                                    <IonLabel>
                                        <h1>{avion.modele}</h1>
                                        <IonNote>Assurance : {avion.compagnie}</IonNote>
                                    </IonLabel>
                                </IonItem>
                            )
                        })
                    }
                </IonList>
            </div>
        );
    }
};

export default TableauAvion;