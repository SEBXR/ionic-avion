import useFetch from "./useFetch";
import { IonLabel,IonItem, IonList, IonNote } from '@ionic/react';
import { IonImg } from '@ionic/react';
import React from "react";
import { IonButton } from '@ionic/react';
import { urlWS } from "./url"; 

interface ContainerProps {}

const DetailsdataContainer: React.FC<ContainerProps> = () => {
    if(localStorage.getItem("token") == null){
        window.location.href = "./../login"
    }
    const url = new URL(window.location.href)
    let id = url.searchParams.get("id")
    let link = urlWS + "/avions/" + id
    console.log(link)
    const  { data }  = useFetch(link)
    if(data == null)
        return <h1>Loading...</h1>
    else{
        return(
         <div className = "container">
            <IonList>
                <IonItem>
                    <IonImg src = {data[0].photo} ></IonImg>
                </IonItem>
                <IonItem>
                    <IonButton>Modifier l'image</IonButton>
                </IonItem>
                <IonItem>
                    <IonLabel>
                        <h1>Modele : {data[0].modele}</h1>
                    </IonLabel>
                </IonItem>
                <IonItem>
                    <IonLabel>
                        <h3>Assurance</h3>
                        <IonNote>{data[0].assurance}</IonNote>
                    </IonLabel>
                </IonItem>
                <IonItem>
                    <IonLabel>
                        <h3>Debut Kilometrage</h3>
                        <IonNote>{data[0].debut_km} km</IonNote>
                    </IonLabel>
                </IonItem>
                <IonItem>
                    <IonLabel>
                        <h3>Fin Kilometrage</h3>
                        <IonNote>{data[0].fin_km} km</IonNote>
                    </IonLabel>
                </IonItem>
            </IonList>
            <IonItem routerLink="/listedata">
                <IonLabel>Retour</IonLabel>
            </IonItem>  
         </div>
        );
    }
};

export default DetailsdataContainer;