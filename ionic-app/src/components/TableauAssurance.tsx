import { IonList, IonLabel , IonNote , IonItem} from "@ionic/react";
import useFetch from "./useFetch";

interface ContainerProps{ }


const TableauAssurance : React.FC<ContainerProps> = () => {
    const { data } = useFetch ("http://172.16.0.207:8080/ass_Expire");
    if (!data) {
        return <h1>Loading...</h1>;
    }else{
        console.log(data)
        return (
          <div>
            <IonList>
              {
                data.map((avion: any) => {
                  return(
                    <IonItem>
                      <IonLabel>
                        <h2>{avion.modele} - {avion.compagnie} - {avion.expiration} mois - Assurance : {avion.assurances}</h2>
                        <IonNote>Date de paiement : {avion.datepaiement}</IonNote>
                        <IonNote>Date d'expiration : {avion.dateexpiration}</IonNote>
                      </IonLabel>
                    </IonItem>
                  )
                })
              }
            </IonList>

          </div>
        );
      }
};
export default TableauAssurance;