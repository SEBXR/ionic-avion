import { IonList, IonLabel , IonNote , IonItem} from "@ionic/react";
import useFetch from "./useFetch";

interface ContainerProps{ }


const TableauAssurance : React.FC<ContainerProps> = () => {
    const { data } = useFetch ("http://172.16.0.207:8080/ass_Expire1Mois");
    if (!data) {
        return <h1>Loading...</h1>;
    }else{
        console.log(data)
        return (
          <div>
            <IonList>
              {
                data.map((avion1: any) => {
                  return(
                    <IonItem>
                      <IonLabel>
                        <h2>{avion1.modele} - {avion1.compagnie} - {avion1.expiration} mois - Assurance : {avion1.assurances}</h2>
                        <IonNote>Date de paiement : {avion1.datepaiement}</IonNote>
                        <IonNote>Date d'expiration : {avion1.dateexpiration}</IonNote>
                      </IonLabel>
                    </IonItem>
                  )
                })
              }
            </IonList>

          </div>
        );
      }
};
export default TableauAssurance;