import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import DetailsAvionContainer from '../components/DetailsAvionContainer';

const DetailsAvion: React.FC = () => {
    return (
        <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonTitle>Detail Avion</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent fullscreen>
          <IonHeader collapse="condense">
            <IonToolbar>
              <IonTitle size="large">Detail Avion</IonTitle>
            </IonToolbar>
          </IonHeader>
          <DetailsAvionContainer />
        </IonContent>
      </IonPage>
    );
};

export default DetailsAvion;