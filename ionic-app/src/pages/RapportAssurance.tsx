import { IonPage, IonHeader, IonContent,IonToolbar,IonTitle } from "@ionic/react";
import TableauAssurance from "../components/TableauAssurance";

const RapportAssurance: React.FC = () => {
    return(
        <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonTitle>Liste Assurance</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent fullscreen>
            <TableauAssurance/>
        </IonContent>
      </IonPage>
    );
};

export default RapportAssurance;