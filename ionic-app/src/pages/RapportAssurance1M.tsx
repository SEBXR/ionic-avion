import { IonPage, IonHeader, IonContent,IonToolbar,IonTitle } from "@ionic/react";
import TableauAssurance1M from "../components/TableauAssurance1M";

const RapportAssurance1M: React.FC = () => {
    return(
        <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonTitle>Liste Assurance</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent fullscreen>
            <TableauAssurance1M/>
        </IonContent>
      </IonPage>
    );
};

export default RapportAssurance1M;