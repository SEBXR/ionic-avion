import { IonPage, IonHeader, IonContent,IonToolbar,IonTitle } from "@ionic/react";
import TableauAssurance3M from "../components/TableauAssurance3M";

const RapportAssurance3M: React.FC = () => {
    return(
        <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonTitle>Liste Assurance</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent fullscreen>
            <TableauAssurance3M/>
        </IonContent>
      </IonPage>
    );
};

export default RapportAssurance3M;