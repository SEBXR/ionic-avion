import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import TableauAvion from '../components/TableauAvion';


const ListeAvion: React.FC = () => {
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>Liste Avion</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent fullscreen>
                <TableauAvion />
            </IonContent>
        </IonPage>
    );
};

export default ListeAvion;