import { Redirect, Route } from 'react-router-dom';
import { IonApp, IonRouterOutlet, setupIonicReact } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import Home from './pages/Home';
import Login from './pages/Login';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import ListeAvion from './pages/ListeAvion';
import DetailsAvionContainer from './pages/DetailsAvion';
import RapportAssurance from './pages/RapportAssurance';
import RapportAssurance1M from './pages/RapportAssurance1M';
import RapportAssurance3M from './pages/RapportAssurance3M';


setupIonicReact();

const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      <IonRouterOutlet>
        <Route exact path="/">
          <Redirect to="/listeAvion" />
        </Route>
        <Route exact path="/listeAvion">
          <ListeAvion />
        </Route>
        <Route exact path="/login">
          <Login />
          </Route>
        <Route exact path="/detailAvion">
          <DetailsAvionContainer />
        </Route>
        <Route exact path="/assurance">
          <RapportAssurance />
        </Route>
        <Route exact path="/assurance1M">
          <RapportAssurance1M />
        </Route>
        <Route exact path="/assurance3M">
          <RapportAssurance3M />
        </Route>
      </IonRouterOutlet>
    </IonReactRouter>
  </IonApp>
);

export default App;
